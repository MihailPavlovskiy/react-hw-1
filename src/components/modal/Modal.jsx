import React, { Component } from "react";
import Button from "../button/Button";
import s from "./Modal.module.scss";
import modalSettings from "./modalList";

export default class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            settings: {},
        };
    }

    componentDidMount() {
        const modal = modalSettings.find(
            (item) => item.modalId === this.props.modalId
        );
        this.setState({ settings: modal.settings });
    }
    render() {
        const { header, closeButton, text, actions } = this.state.settings;
        return (
            <div className={s.Modal} onClick={this.props.closeModal}>
                <div
                    className={s.ModalContent}
                    onClick={(e) => e.stopPropagation()}
                >
                    <div className={s.ModalHeader}>
                        {header && header}
                        {closeButton && closeButton && (
                            <Button
                                text="X"
                                background={'red'}
                                onClick={this.props.closeModal}
                            />
                        )}
                    </div>
                    <div className={s.ModalBody}>{text && text}</div>
                    <div className={s.ModalFooter}>
                        {actions &&
                            actions.map((item, index) => (
                                <Button
                                    backgroundColor={item.backgroundColor}
                                    text={item.text}
                                    onClick={
                                        item.type === "submit"
                                            ? () => this.props.submit(this.props.data)
                                            : this.props.closeModal
                                    }
                                    key={Date.now() + index}
                                />
                            ))}
                    </div>
                </div>
            </div>
        );
    }
}
