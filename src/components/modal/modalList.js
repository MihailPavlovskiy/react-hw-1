const modalList = [
    {
        modalId: "firstModal",
        settings: {
            header: "Header for first modal",
            closeButton: true,
            text: "Text for first modal",
            actions: [
                {
                    type: "submit",
                    text: "Confirm",
                },
                {
                    type: "cancel",
                    text: "Cancel",
                },
            ],
        },
    },
    {
        modalId: "secondModal",
        settings: {
            header: "Header for second modal",
            closeButton: false,
            text: "Text for second modal",
            actions: [
                {
                    type: "submit",
                    text: "Confirm",
                },
                {
                    type: "cancel",
                    text: "Cancel",
                },
            ],
        },
    },
];

export default modalList;
