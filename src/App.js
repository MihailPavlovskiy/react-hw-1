import React, { Component } from "react";
import s from './App.css'
import Modal from "./components/modal/Modal";
import Button from "./components/button/Button";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: {
        visible: false,
        modalId: null,
        submit: null,
        data: null,
      },
    };
  }

  openModal = (modalId, submit = null, data = null) => {
    this.setState({
      modal: {
        visible: true,
        modalId,
        submit,
        data,
      },
    });
  };

  closeModal = () => {
    this.setState({
      modal: {
        visible: false,
        modalId: null,
        submit: null,
        data: null,
      },
    });
  };

  onSubmitFirstModal = (data = "") => {
    alert(data ? data : "Empty 'data'");
    this.closeModal();
  };

  onSubmitSecondModal = (data = "") => {
    alert(data ? data : "Empty 'data'");
    this.closeModal();
  };

  render() {
    return (
        <div className={s.App}>
          <Button
              background={'darkgrey'}
              text="Open first modal"
              onClick={() => {
                this.openModal(
                    "firstModal",
                    this.onSubmitFirstModal,
                    "Some first modal data"
                );
              }}
          />
          <Button
              background={'lightblue'}
              text="Open second modal"
              onClick={() =>
                  this.openModal(
                      "secondModal",
                      this.onSubmitSecondModal,
                      "Some second modal data"
                  )
              }
          />
          {this.state.modal.visible && (
              <Modal
                  modalId={this.state.modal.modalId}
                  submit={this.state.modal.submit}
                  data={this.state.modal.data}
                  closeModal={this.closeModal}
              />
          )}
        </div>
    );
  }
}

export default App;

